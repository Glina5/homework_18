﻿#include <iostream>
#include <string>

template <class Type> class Stack
{
private:
    Type* NewStack = nullptr;
    Type* OldStack = nullptr;
    int length = 0;
    Type popNum;
    void Newcopy()
    {
        for (int N = 0; N < length - 1; N++)
        {
            *(NewStack + N) = *(OldStack + N);
        }
    };
    void Oldcopy()
    {
        for (int N = 0; N < length - 1; N++)
        {
            *(OldStack + N) = *(NewStack + N);
        }
    };
public:

    void pop()
    {
        if (length <= 0)
        {
            std::cout << "Stack is empty \n";
            return;
        }
        popNum = *(OldStack + (length-1));
        NewStack = new Type[length-1];
        Newcopy();
        delete[] OldStack;
        OldStack = new Type[length-1];
        Oldcopy();
        delete[] NewStack;
        length--;
        std::cout << popNum << '\n';
    };
    void push(const Type number)
    {
        length++;
        NewStack = new Type[length];
        Newcopy();
        delete[] OldStack;
        OldStack = new Type[length];
        Oldcopy();
        delete[] NewStack;
        *(OldStack + (length - 1)) = number;
    };
};

int main()
{
    Stack <int> S_int;
    S_int.pop();
    S_int.push(10);
    S_int.push(20);
    S_int.push(50);
    S_int.pop();
    S_int.pop();
    S_int.pop();
    S_int.pop();
    
    std::cout << '\n';

    Stack <float> S_float;
    S_float.pop();
    S_float.push(10.1f);
    S_float.push(20.2f);
    S_float.push(50.5f);
    S_float.pop();
    S_float.pop();
    S_float.pop();
    S_float.pop();

    std::cout << '\n';

    Stack <double> S_double;
    S_double.pop();
    S_double.push(10.1);
    S_double.push(20.2);
    S_double.push(50.5);
    S_double.pop();
    S_double.pop();
    S_double.pop();
    S_double.pop();

    std::cout << '\n';

    Stack <std::string> S_string;
    S_string.pop();
    S_string.push("The courses!");
    S_string.push("For");
    S_string.push("Thanks");
    S_string.pop();
    S_string.pop();
    S_string.pop();
    S_string.pop();

    std::cout << '\n';

    return 0;
}
